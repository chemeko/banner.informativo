export interface DataBannerInformativo {
  urlDestino: string;
  texto: string;
  colorTexto: string;
  colorFondo: string;
  textoAuxiliar: string;
}
