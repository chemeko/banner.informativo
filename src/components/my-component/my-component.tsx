import { Component, State, Element, h } from '@stencil/core';
import { DataBannerInformativo } from './dataBannerInformativo';

@Component({
  tag: 'banner-informativo-component',
  assetsDirs: ['assets'],
  styleUrl: 'my-component.scss',
  shadow: true,
})

export class MyComponent {
  @State() dataBannerInformativo: DataBannerInformativo =
    {} as DataBannerInformativo;
  @Element() el: HTMLElement;
  @State() cosita: string = 'style="background: rgb(244, 46, 98);"';

  async componentWillLoad() {
    await fetch('https://api-interno.www.gov.co/api/home/BannerInformativo')
      .then((response: Response) => response.json())
      .then(response => {
        this.dataBannerInformativo.urlDestino = response.data.urlDestino;
        this.dataBannerInformativo.texto = response.data.texto;
        this.dataBannerInformativo.colorTexto = response.data.colorTexto;
        this.dataBannerInformativo.colorFondo = response.data.colorFondo;
        this.dataBannerInformativo.textoAuxiliar = response.data.textoAuxiliar;
      });
  }

  componentDidLoad() {
    var elementInShadowDom = this.el.shadowRoot.getElementById("banner-back");
    elementInShadowDom.style.background = this.dataBannerInformativo.colorFondo;

    var elementInShadowDom = this.el.shadowRoot.getElementById("color-blanco-1");
    elementInShadowDom.style.color = this.dataBannerInformativo.colorTexto;

    var elementInShadowDom = this.el.shadowRoot.getElementById("color-blanco-2");
    elementInShadowDom.style.color = this.dataBannerInformativo.colorTexto;

    var elementInShadowDom = this.el.shadowRoot.getElementById("color-blanco-3");
    elementInShadowDom.style.color = this.dataBannerInformativo.colorTexto;
  }

  render() {
    return (
      <div>
        <div id="banner-back">
          <div class="container">
            <div class="banner-informativo align-items-center row">
              <div class="col-sm-12 col-md-9">
                <a id="color-blanco-1" class="banner-informativo-text" role="link" href={this.dataBannerInformativo.urlDestino} rel="noreferrer" target="">
                  <span
                    class="icono-alerta">&nbsp;&nbsp;&nbsp;</span>{this.dataBannerInformativo.texto}&nbsp;&nbsp;&nbsp;
                </a>
              </div>
              <div class="col-sm-12 col-md-3 banner-informativo-recordarmelo">
                <a href="javascript:void(0)" role="button" id="color-blanco-2">
                  <u id="color-blanco-3">{this.dataBannerInformativo.textoAuxiliar}</u>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

