import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';
export const config: Config = {
  plugins: [sass()],
  bundles: [
    { components: ['banner-informativo-component'] },
  ],
  namespace: 'banner-informativo-stencil',
  outputTargets: [
    {
      type: 'dist-custom-elements',
      copy: [
        {
          src: '**/*.{jpg,png}',
          dest: 'dist/components/assets',
          warn: true,
        },
      ],
    },
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
    },
  ],
  testing: {
    browserHeadless: 'new',
  },
};
